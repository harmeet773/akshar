<!DOCTYPE html>
<html lang="en" >
   <head>
      <meta name="viewport"         
         content="width=device-width,      
         initial-scale=1">
      <meta charset="utf-8">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
      <link href="bootstrap.min.css" rel="stylesheet">
      <link href="floating-labels.css" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> 
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
      <style>
         .body {
         background: transperent ;
         }
         .heading{
         text-align:center;
         font-size:33px;
         padding-top:50px;
         }
         .form-control {
         border: none;
         border-bottom: 1px solid ;
         outline: none;
         }
         .form-group{
         padding-left:30px;
         padding-right:30px;
         }
         .btn{ 
         margin-left:422px;
         margin-top:40px;
         padding-left:50px;
         padding-right:50px;
         padding-bottom:10px;
         padding-top:10px;
         font-size:25px;
         }
         .form-control-custom::-webkit-file-upload-button { 
         visibility: hidden;
         }
         .form-control-custom {
         content: 'Select some files';
         display: inline-block; 
         background: linear-gradient(to top, #f9f9f9, #e3e3e3);
         border: 1px solid #999;
         border-radius: 5px;
         padding: 5px 8px; 
         outline: none;
         white-space: nowrap; -webkit-user-select: none; 
         cursor: pointer; 
         text-shadow: 1px 1px #fff; 
         font-weight: 700; 
         font-size: 10pt;
         }
         .form-control-custom:hover::before { 
         border-color: black;
         } 
         .form-check{
         margin-left:20px;
         }
         @media only screen and (max-width:600px){
         .btn {
         width:90%;
         margin:20px;
         }
         }
      </style>
     
   </head>
   <body>

      <div>
         <h2 class="heading"><b>Submit entries here</b></h2>
      </div>
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" 
         enctype="multipart/form-data">
         <?php
            echo " <h4 style='text-align:center; color:red '  >   $remark </h4> "  ;
            ?>
         <div class="d-sm-flex  flex-sm-row mb-3">
            <div class="p-2  flex-fill ">
               <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email"
                     required>
               </div>
            </div>
            <div class="p-2  flex-fill ">
               <div class="form-group">
                  <label for="fname">Name:</label>
                  <input type="fullname" class="form-control" id="fname"  name="fname" required>
               </div>
            </div>
         </div>
         <div class="d-sm-flex  flex-sm-row mb-3">
            <div class="p-2  flex-fill">
               <div class="form-group">
                  <label for="clgname">Collage:</label>
                  <input type="clgname" class="form-control" id="clgname" name="clgname" required>
               </div>
            </div>
            <div class="p-2  flex-fill">
               <div class="form-group">
                  <label for="mob">Contact No:</label>
                  <input type="number" class="form-control" id="mob" name="mob" required>
               </div>
            </div>
         </div>
         <div class="d-sm-flex  flex-sm-row mb-3">
            <div class="p-2 flex-fill">
               <div class="form-group">
                  <label for="branch">BRANCH:</label><br>
                  <select name="branch" id="branch" class="form-control">
                     <option value="EX">EX</option>
                     <option value="CSE">CSE</option>
                     <option value="AUTO">AUTO</option>
                     <option value="CIVIL">CIVIL</option>
                     <option value="MECH">MECH</option>
                     <option value="PCT">PCT</option>
                     <option value="IT">IT</option>
                  </select>
               </div>
            </div>
            <div class="p-2  flex-fill">
               <div class="form-group">
                  <label for="year">Year Of study:</label><br>
                  <select name="year" id="year" class="form-control">
                     <option value="1">1st</option>
                     <option value="2">2nd</option>
                     <option value="3">3rd</option>
                     <option value="4">4th</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="d-sm-flex  flex-sm-row mb-3">
            <div class="p-2  flex-fill">
               <div class="form-group">
                  <label for="text">Genre:</label>
                  <input type="text" class="form-control" id="gen" name="gen">
               </div>
            </div>
            <div class="p-2 flex-fill">
               <div class="form-group">
                  <label for="file">File:</label>
                  <p><I>Please upload your content in text formats (doc, docx, rtf, txt etc.) for textual matter and with JPEG, JPG or PNG extensions for artwork.</I></p>
                  <input type="file" name="file" class="form-control-custom" id="file">
               </div>
            </div>
         </div>
         <div class="d-sm-flex  flex-sm-row mb-3">
            <div class="p-2  flex-fill">
               <div class="form-group">
                  <label for="suggestion">Any Suggestion:</label>
                  <p>Tell us how we can improve our magazine. We will be glad to hear from you!</p>
                  <input type="text" class="form-control" id="suggestion" name="suggestion">
               </div>
            </div>
            <div class="p-2  flex-fill">
               <div class="form-group form-check">
                  <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" name="remember" checked> Remember me
                  </label>
               </div>
            </div>
         </div>
         <button type="submit" class="btn btn-primary" name="submit" id="submit" >Submit</button>
      </form>
      <hr >


       <?php 
         $remark="" ;
         if(isset($_POST["submit"])){
             
             
         
         
             if(!empty($_POST["email"])&&!empty($_POST["fname"]) &&!empty($_POST["clgname"])
             &&!empty($_POST["mob"])&&!empty($_POST["branch"])&&!empty($_POST["year"])
             &&!empty($_POST["gen"])&&!empty($_POST["suggestion"])&&!empty($_FILES['file']))
             {
                
                
                
                 $Connection=mysqli_connect('localhost','username','password','db_name' );
                
                 function test_input($data) {
                     $data = trim($data);
                     $data = stripslashes($data);
                     $data = htmlspecialchars($data);
                     return $data;
                     }
             
                     $email= test_input($_POST["email"]);
         
                     $fname= test_input($_POST["fname"]);
         
                     $clgname= test_input($_POST["clgname"]);
                     $mob= test_input($_POST["mob"]);
                     $branch= test_input($_POST["branch"]);
                     $year= test_input($_POST["year"]); 
                     $gen= test_input($_POST["gen"]);
                     $suggestion= test_input($_POST["suggestion"]);
                     $fileName =test_input($_FILES['file']['name']);
                     $target_path = "e:/";  
                     $target_path = $target_path.basename( $_FILES['file']['name']); 
                     $check=   move_uploaded_file($_FILES['file']['tmp_name'], $target_path);
                     
                     $Query="INSERT INTO table(email,fname,clgname,mob,branch,year,gen,suggestion,filename)
                 VALUES('$email','$fname','$clgname','$mob','$branch','$year','$gen','$suggestion',$fileName)";
                     $check2=mysqli_query($Connection,$Query);
         
                     if($check&&$check2){
         
                       echo " <script>
                       swal('Good job!', 'You clicked the button!', 'success');
         
                         </script> ";
                           //sweet alert was not working so 'window.alert' was added 
                         echo " <script> window.alert('sweet alert was not working so i used old method.') </script> ";
                         
                     }
         
             }
             else{
                 $remark  = " All fields are required";
         
             }
         
         
         }
         
         
         
         ?>
      
   </body>
</html>